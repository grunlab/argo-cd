[![App Status](https://argo-cd-status.grunlab.net/badge?name=argo-cd&revision=true&showAppName=true)](https://argo-cd.lan.grunlab.net/applications/argo-cd)

# GrunLab Argo CD

Argo CD deployment on Kubernetes.

Docs: https://docs.grunlab.net/install/argo-cd.md

GrunLab project(s) using this service:
- [grunlab/argo-cd][argo-cd]
- [grunlab/argo-events][argo-events]
- [grunlab/argo-workflows][argo-workflows]
- [grunlab/calico][calico]
- [grunlab/deluge][deluge]
- [grunlab/dl][dl]
- [grunlab/docs][docs]
- [grunlab/gerbera][gerbera]
- [grunlab/gitlab-runner][gitlab-runner]
- [grunlab/joal][joal]
- [grunlab/mariadb-operator][mariadb-operator]
- [grunlab/metallb][metallb]
- [grunlab/metrics-server][metrics-server]
- [grunlab/navidrome][navidrome]
- [grunlab/nextcloud][nextcloud]
- [grunlab/phpmyadmin][phpmyadmin]
- [grunlab/rook-ceph][rook-ceph]
- [grunlab/sealed-secrets][sealed-secrets]
- [grunlab/storj][storj]
- [grunlab/tor][tor]
- [grunlab/traefik][traefik]
- [grunlab/vigixplorer][vigixplorer]

[argo-cd]: <https://gitlab.com/grunlab/argo-cd>
[argo-events]: <https://gitlab.com/grunlab/argo-events>
[argo-workflows]: <https://gitlab.com/grunlab/argo-workflows>
[calico]: <https://gitlab.com/grunlab/calico>
[deluge]: <https://gitlab.com/grunlab/deluge>
[dl]: <https://gitlab.com/grunlab/dl>
[docs]: <https://gitlab.com/grunlab/docs>
[gerbera]: <https://gitlab.com/grunlab/gerbera>
[gitlab-runner]: <https://gitlab.com/grunlab/gitlab-runner>
[joal]: <https://gitlab.com/grunlab/joal>
[mariadb-operator]: <https://gitlab.com/grunlab/mariadb-operator>
[metallb]: <https://gitlab.com/grunlab/metallbdl>
[metrics-server]: <https://gitlab.com/grunlab/metrics-server>
[navidrome]: <https://gitlab.com/grunlab/navidrome>
[nextcloud]: <https://gitlab.com/grunlab/nextcloud>
[phpmyadmin]: <https://gitlab.com/grunlab/phpmyadmin>
[rook-ceph]: <https://gitlab.com/grunlab/rook-ceph>
[sealed-secrets]: <https://gitlab.com/grunlab/sealed-secrets>
[storj]: <https://gitlab.com/grunlab/storj>
[tor]: <https://gitlab.com/grunlab/tor>
[traefik]: <https://gitlab.com/grunlab/traefik>
[vigixplorer]: <https://gitlab.com/grunlab/vigixplorer>

