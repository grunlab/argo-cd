argocd account bcrypt --password <password>
kubectl -n argo-cd patch secret argocd-secret -p '{"stringData": {"admin.password": "<bcrypt>", "admin.passwordMtime": "'$(date +%FT%T%Z)'"}}'

